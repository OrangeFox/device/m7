**OrangeFox device tree for HTC One m7 (Universal)**

This is a recovery-only device tree for the HTC One (m7u / m7ul / m7wls / m7wlv).

**Quick build instructions:**

    . build/envsetup.sh
    lunch omni_m7-eng
    mka recoveryimage
