LOCAL_PATH := $(call my-dir)

FLASH_IMAGE_TARGET ?= $(PRODUCT_OUT)/recovery.tar

BOARD_MKBOOTIMG_ARGS += --ramdisk_offset 0x01800000

$(recovery_ramdisk): $(recovery_uncompressed_ramdisk)
	@echo -e ${CL_GRN}"----- Compressing recovery ramdisk (xz) ------"${CL_RST}
	$(hide) xz -9c --format=lzma --lzma1=dict=16MiB $< > $@

$(INSTALLED_RECOVERYIMAGE_TARGET): $(MKBOOTIMG) $(INSTALLED_DTIMAGE_TARGET) $(recovery_kernel) $(recovery_ramdisk)
	@echo -e ${CL_GRN}"----- Making recovery image ------"${CL_RST}
	$(hide) $(MKBOOTIMG) $(INTERNAL_RECOVERYIMAGE_ARGS) $(BOARD_MKBOOTIMG_ARGS) --output $@ --ramdisk $(recovery_ramdisk)
	@echo -e ${CL_CYN}"Made recovery image: $@"${CL_RST}
#
